# KickMount

This application lets you eject all external volumes at once or single volumes from the menu bar.

<img src="imgs/screenshot_1.png" style="width: 50%">
<img src="imgs/screenshot_2.png" style="width: 50%">


## Installation

### With Homebrew

```
brew cask install https://raw.githubusercontent.com/m12n/kickmount/master/kickmount.rb
```

### Download

Download the latest [version (1.0.2) here][1].
Then drag the application to your Application folder.


## Development

This application is a Java application and uses Maven to build.

```
mvn clean package
```

Creates a DMG file in the `/target` directory of the project, containing an executable Mac 
application.

### Documentation

Edid documentation in the `/src/main/docs` directory of the project. The Maven build automatically
copies all files to the root directory.
 

### Deployment

To deploy a release, you need to have the Python library `requests` installed.

``` 
pip install requests
```

Then you can run the build script, a Python script is copied to the root directory. Run it to 
release the software.

Prerequisites:

- Commit everything to your development branch
- Merge the development branch to master (either using pull requests or Git command line)
- Check out the latest Git master branch
- Install `requests` library

```
./release.py
```

To revert the release, if anything went wrong, run the following commands:

```
PROJECT_VERSION=`mvn -q -Dexec.executable="echo" -Dexec.args='1.0.2' --non-recursive org.codehaus.mojo:exec-maven-plugin:1.3.1:exec` && \
git tag -d $PROJECT_VERSION && \
git push origin :$PROJECT_VERSION && \
git push github :$PROJECT_VERSION && \
git reset --hard HEAD~1
```


[1]: https://github.com/m12n/kickmount/releases/download/1.0.2/kickmount-1.0.2.dmg
