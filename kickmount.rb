cask 'kickmount' do
  version '1.0.2'
  sha256 'd0c6bcb346d6e83afe3ff40b18505edfb13d8c02243496e2ffd45c59b99ab3e7'

  url "https://github.com/m12n/kickmount/releases/download/1.0.2/kickmount-1.0.2.dmg"
  name 'KickMount'
  homepage 'https://github.com/m12n/kickmount'

  app 'KickMount.app'

  caveats do
    depends_on_java('8')
  end

  zap delete: [
    '~/Library/Application Support/KickMount',
    '~/Library/Logs/KickMount',
    '~/Library/LaunchAgents/org.m12n.kickmount.plist'
  ]
end
