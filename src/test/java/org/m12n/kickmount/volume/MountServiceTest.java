package org.m12n.kickmount.volume;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.m12n.incubator.process.ProcessManager;
import org.m12n.util.factory.Factory;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MountServiceTest {
    @Mock
    private ProcessManager processManager;
    @Mock
    private Process process;

    @Test
    public void unmount_withNoError_success() throws Exception {
        assertUnmount(true, 0, false);
    }

    @Test
    public void unmount_withTimoutReached_fails() throws Exception {
        assertUnmount(false, 0, true);
    }

    @Test
    public void unmount_withErrorCode_fails() throws Exception {
        assertUnmount(true, 1, true);
    }

    @Test
    public void unmount_withIOException_fails() throws Exception {
        final String volumeName = "test123";
        when(processManager.exec("diskutil", "eject", volumeName)).thenThrow(IOException.class);

        runUnmount(volumeName, true);
    }

    @Test
    public void unmount_withInterruptedException_fails() throws Exception {
        final String volumeName = "test123";
        when(processManager.exec("diskutil", "eject", volumeName)).thenReturn(process);
        when(process.waitFor(10, TimeUnit.SECONDS)).thenThrow(InterruptedException.class);

        runUnmount(volumeName, true);
    }

    private void assertUnmount(final boolean waitForResult, final int exitValueresult, final boolean isEjectFailedExpected) throws IOException, InterruptedException {
        final String volumeName = "test123";
        when(processManager.exec("diskutil", "eject", volumeName)).thenReturn(process);
        when(process.waitFor(10, TimeUnit.SECONDS)).thenReturn(waitForResult);
        when(process.exitValue()).thenReturn(exitValueresult);

        runUnmount(volumeName, isEjectFailedExpected);
    }

    private void runUnmount(final String volumeName, final boolean isEjectFailedExpected) {
        final Volume volume = new Volume(volumeName);
        assertThat(volume.isEjectFailed(), is(false));
        Factory.define(ProcessManager.class, processManager);
        Factory.create(MountService.class).unmount(volume);
        assertThat(volume.isEjectFailed(), is(isEjectFailedExpected));
    }

    @Test
    public void externalVolumes() throws Exception {
        final String plist = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n" +
                "<plist version=\"1.0\">\n" +
                "<dict>\n" +
                "\t<key>AllDisks</key>\n" +
                "\t<array>\n" +
                "\t\t<string>disk2</string>\n" +
                "\t\t<string>disk2s1</string>\n" +
                "\t\t<string>disk7</string>\n" +
                "\t\t<string>disk7s1</string>\n" +
                "\t\t<string>disk7s2</string>\n" +
                "\t</array>\n" +
                "\t<key>AllDisksAndPartitions</key>\n" +
                "\t<array>\n" +
                "\t\t<dict>\n" +
                "\t\t\t<key>Content</key>\n" +
                "\t\t\t<string>GUID_partition_scheme</string>\n" +
                "\t\t\t<key>DeviceIdentifier</key>\n" +
                "\t\t\t<string>disk2</string>\n" +
                "\t\t\t<key>Partitions</key>\n" +
                "\t\t\t<array>\n" +
                "\t\t\t\t<dict>\n" +
                "\t\t\t\t\t<key>Content</key>\n" +
                "\t\t\t\t\t<string>Apple_HFS</string>\n" +
                "\t\t\t\t\t<key>DeviceIdentifier</key>\n" +
                "\t\t\t\t\t<string>disk2s1</string>\n" +
                "\t\t\t\t\t<key>DiskUUID</key>\n" +
                "\t\t\t\t\t<string>B02D787D-0430-4B9E-874D-0D177A2123A2</string>\n" +
                "\t\t\t\t\t<key>MountPoint</key>\n" +
                "\t\t\t\t\t<string>/Volumes/MY_DISK</string>\n" +
                "\t\t\t\t\t<key>Size</key>\n" +
                "\t\t\t\t\t<integer>3784704</integer>\n" +
                "\t\t\t\t\t<key>VolumeName</key>\n" +
                "\t\t\t\t\t<string>MY_DISK</string>\n" +
                "\t\t\t\t\t<key>VolumeUUID</key>\n" +
                "\t\t\t\t\t<string>113EEE5F-B3F3-30C5-AA55-458DE81B98BE</string>\n" +
                "\t\t\t\t</dict>\n" +
                "\t\t\t</array>\n" +
                "\t\t\t<key>Size</key>\n" +
                "\t\t\t<integer>3825152</integer>\n" +
                "\t\t</dict>\n" +
                "\t\t<dict>\n" +
                "\t\t\t<key>Content</key>\n" +
                "\t\t\t<string>Apple_partition_scheme</string>\n" +
                "\t\t\t<key>DeviceIdentifier</key>\n" +
                "\t\t\t<string>disk7</string>\n" +
                "\t\t\t<key>Partitions</key>\n" +
                "\t\t\t<array>\n" +
                "\t\t\t\t<dict>\n" +
                "\t\t\t\t\t<key>Content</key>\n" +
                "\t\t\t\t\t<string>Apple_partition_map</string>\n" +
                "\t\t\t\t\t<key>DeviceIdentifier</key>\n" +
                "\t\t\t\t\t<string>disk7s1</string>\n" +
                "\t\t\t\t\t<key>Size</key>\n" +
                "\t\t\t\t\t<integer>32256</integer>\n" +
                "\t\t\t\t</dict>\n" +
                "\t\t\t\t<dict>\n" +
                "\t\t\t\t\t<key>Content</key>\n" +
                "\t\t\t\t\t<string>Apple_HFS</string>\n" +
                "\t\t\t\t\t<key>DeviceIdentifier</key>\n" +
                "\t\t\t\t\t<string>disk7s2</string>\n" +
                "\t\t\t\t\t<key>Size</key>\n" +
                "\t\t\t\t\t<integer>19730432</integer>\n" +
                "\t\t\t\t\t<key>VolumeName</key>\n" +
                "\t\t\t\t\t<string>Flash Player</string>\n" +
                "\t\t\t\t\t<key>VolumeUUID</key>\n" +
                "\t\t\t\t\t<string>CDE9ED48-3837-3DFE-880F-9019446DEC57</string>\n" +
                "\t\t\t\t</dict>\n" +
                "\t\t\t</array>\n" +
                "\t\t\t<key>Size</key>\n" +
                "\t\t\t<integer>19763200</integer>\n" +
                "\t\t</dict>\n" +
                "\t</array>\n" +
                "\t<key>VolumesFromDisks</key>\n" +
                "\t<array>\n" +
                "\t\t<string>MY_DISK</string>\n" +
                "\t</array>\n" +
                "\t<key>WholeDisks</key>\n" +
                "\t<array>\n" +
                "\t\t<string>disk2</string>\n" +
                "\t\t<string>disk7</string>\n" +
                "\t</array>\n" +
                "</dict>\n" +
                "</plist>\n";

        when(processManager.exec("diskutil", "list", "-plist", "external")).thenReturn(process);
        when(process.getInputStream()).thenReturn(new ByteArrayInputStream(plist.getBytes()));
        when(process.waitFor()).thenReturn(0);

        Factory.define(ProcessManager.class, processManager);
        final MountService mountService = Factory.create(MountService.class);

        final Collection<Volume> volumes = mountService.externalVolumes();
        assertThat(volumes.size(), is(1));
        assertThat(volumes.iterator().next(), is(new Volume("MY_DISK")));
    }

    @Test
    public void externalVolumes_withIOException_empty() throws Exception {
        when(processManager.exec("diskutil", "list", "-plist", "external")).thenThrow(IOException.class);

        Factory.define(ProcessManager.class, processManager);
        final MountService mountService = Factory.create(MountService.class);

        final Collection<Volume> volumes = mountService.externalVolumes();
        assertThat(volumes.size(), is(0));
    }
}
