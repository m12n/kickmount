package org.m12n.incubator.version;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class GithubVersionTest {
    @Test
    public void latestVersion() throws Exception {
        final GithubVersion githubVersion = new GithubVersion();

        final SemanticVersion version = githubVersion.latestVersion();

        assertNotNull(version);
    }
}
