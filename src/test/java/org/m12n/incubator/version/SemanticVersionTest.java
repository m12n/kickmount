package org.m12n.incubator.version;

import org.junit.Test;

import java.util.function.Function;

import static org.junit.Assert.assertTrue;

public class SemanticVersionTest {
    @Test(expected = IllegalArgumentException.class)
    public void create_noPatchVersion_throwsException() {
        new SemanticVersion("12.34");
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_noMinorVersion_throwsException() {
        new SemanticVersion("12");
    }

    @Test(expected = IllegalArgumentException.class)
    public void create_illegalFormat_throwsException() {
        new SemanticVersion("abc.def.ghi");
    }

    @Test
    public void compareSemanticVersion() {
        assertCompare(new SemanticVersion("1.0.0"), new SemanticVersion("0.0.7"), c -> c > 0, " > ");
        assertCompare(new SemanticVersion("1.0.0"), new SemanticVersion("0.9.9"), c -> c > 0, " > ");
        assertCompare(new SemanticVersion("1.0.1"), new SemanticVersion("1.0.0"), c -> c > 0, " > ");
        assertCompare(new SemanticVersion("1.0.1-rc2"), new SemanticVersion("1.0.1-rc1"), c -> c > 0, " > ");
        assertCompare(new SemanticVersion("1.0.1"), new SemanticVersion("1.0.1-SNAPSHOT"), c -> c > 0, " > ");
        assertCompare(new SemanticVersion("1.0.1"), new SemanticVersion("1.0.1"), c -> c == 0, " == ");
    }

    private void assertCompare(final SemanticVersion s1, final SemanticVersion s2, final Function<Integer, Boolean> expFn, final String expOp) {
        final int comp = s1.compareTo(s2);
        assertTrue(s1 + expOp + s2 + " = " + comp, expFn.apply(comp));
    }
}
