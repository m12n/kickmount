package org.m12n.incubator.properties;

import org.junit.Test;
import org.m12n.util.factory.Factory;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AppPropertiesTest {
    @Test
    public void loadProperties() {
        final AppProperties properties = Factory.create(AppProperties.class);

        assertThat(properties.getName(), is("KickMount"));
    }
}
