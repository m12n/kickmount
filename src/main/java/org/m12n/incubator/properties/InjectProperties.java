package org.m12n.incubator.properties;

import org.m12n.util.factory.ProcessedBy;
import org.m12n.util.factory.TypeProcessor;

import java.io.InputStream;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.util.Properties;

import static org.m12n.util.Exceptions.sneakyThrows;

@ProcessedBy(InjectProperties.Processor.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface InjectProperties {
    String name();

    String prefix();

    class Processor extends TypeProcessor<Object, InjectProperties> {
        @Override
        protected void processType(final Object obj, final InjectProperties annotation) throws Exception {
            final Properties props = loadProperties(annotation.name());
            final Field[] fields = obj.getClass().getDeclaredFields();
            for (final Field field : fields) {
                if (field.getType().equals(String.class)) {
                    final String propName = annotation.prefix() + "." + field.getName();
                    final String property = props.getProperty(propName);
                    field.setAccessible(true);
                    field.set(obj, property);
                }
            }
        }

        private Properties loadProperties(final String name) {
            final Properties props = new Properties();
            final InputStream in = getClass().getResourceAsStream("/" + name + ".properties");
            sneakyThrows(() -> props.load(in));
            return props;
        }
    }
}
