package org.m12n.incubator.properties;

import org.m12n.incubator.version.SemanticVersion;

@SuppressWarnings("unused")
@InjectProperties(name = "app", prefix = "app")
public class AppProperties {
    private String name;
    private String id;
    private String version;
    private String group;

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public SemanticVersion getVersion() {
        return new SemanticVersion(version);
    }

    public String getGroup() {
        return group;
    }
}
