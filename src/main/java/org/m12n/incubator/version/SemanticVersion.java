package org.m12n.incubator.version;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SemanticVersion implements Comparable<SemanticVersion> {
    private static final Pattern REGEX = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+)(.*)");
    private final int major;
    private final int minor;
    private final int patch;
    private final String suffix;

    public SemanticVersion(final String version) {
        final Matcher m = REGEX.matcher(version);
        if (!m.matches()) {
            throw new IllegalArgumentException("Illegal version syntax: " + version);
        }
        major = Integer.parseInt(m.group(1));
        minor = Integer.parseInt(m.group(2));
        patch = Integer.parseInt(m.group(3));
        suffix = m.group(4);
    }

    @Override
    public int compareTo(final SemanticVersion s) {
        int comp = Integer.compare(major, s.major);
        if (comp == 0) {
            comp = Integer.compare(minor, s.minor);
            if (comp == 0) {
                comp = Integer.compare(patch, s.patch);
                if (comp == 0) {
                    if (suffix.equals("")) {
                        if (s.suffix.equals("")) {
                            return 0;
                        }
                        return 1;
                    }
                    comp = suffix.compareTo(s.suffix);
                }
            }
        }
        return comp;
    }

    @Override
    public String toString() {
        return String.format("%d.%d.%d%s", major, minor, patch, suffix);
    }
}
