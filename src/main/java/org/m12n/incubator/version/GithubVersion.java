package org.m12n.incubator.version;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.m12n.util.Exceptions.sneakyThrows;

public class GithubVersion {
    private static final URL GITHUB_URL = sneakyThrows(() -> new URL("https://api.github.com/repos/m12n/kickmount/tags?page=1&per_page=1"));
    private static final Pattern NAME_REGEX = Pattern.compile(".*\"name\":\"([^\\\"]*)\".*");
    private static final Logger LOG = LoggerFactory.getLogger(GithubVersion.class);

    public SemanticVersion latestVersion() {
        try {
            final URLConnection con = GITHUB_URL.openConnection();
            con.setRequestProperty("Accept", "application/vnd.github.v3+json");
            con.connect();
            try (final InputStream in = con.getInputStream()) {
                return new BufferedReader(new InputStreamReader(in))
                        .lines()
                        .map(NAME_REGEX::matcher)
                        .filter(Matcher::matches)
                        .map(m -> m.group(1))
                        .map(SemanticVersion::new)
                        .findFirst()
                        .orElse(null);
            }
        } catch (final IOException | IllegalArgumentException e) {
            LOG.error("Unable to load version.", e);
            return null;
        }
    }
}
