package org.m12n.incubator.version;

import org.m12n.incubator.process.ProcessManager;
import org.m12n.incubator.properties.AppProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import javax.inject.Inject;

public class GithubVersionCheck {
    private static final Logger LOG = LoggerFactory.getLogger(GithubVersionCheck.class);

    @Inject
    private AppProperties appProperties;
    @Inject
    private ProcessManager processManager;

    private final GithubVersion githubVersion = new GithubVersion();
    private SemanticVersion latestVersion;

    public boolean updateAvailable() {
        latestVersion = githubVersion.latestVersion();
        return latestVersion != null && latestVersion.compareTo(appProperties.getVersion()) > 0;
    }

    public void downloadLatestVersion() {
        final String appName = appProperties.getId();
        final String appVersion = latestVersion.toString();
        final String url = String.format("https://github.com/m12n/%1$s/releases/download/%2$s/%1$s-%2$s.dmg", appName, appVersion);
        LOG.info("Downloading update {}.", url);
        try {
            final Process process = processManager.exec("open", url);
            final int exitCode = process.waitFor();
            if (exitCode != 0) {
                LOG.error("Unable to download ({}).", exitCode);
            }
        } catch (final IOException | InterruptedException e) {
            LOG.error("Unable to download.", e);
        }
    }
}
