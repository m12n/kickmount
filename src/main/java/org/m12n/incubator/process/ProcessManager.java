package org.m12n.incubator.process;

import java.io.IOException;

public class ProcessManager {
    public Process exec(final String... args) throws IOException {
        return Runtime.getRuntime().exec(args);
    }
}
