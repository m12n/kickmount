package org.m12n.incubator.macos;

import org.m12n.util.factory.Factory;

public abstract class MacOsApplication {
    static {
        hideDockIcon();
    }

    protected static <A extends MacOsApplication> void launch(final Class<A> appClass) throws Exception {
        Factory.create(appClass).run();
    }

    private static void hideDockIcon() {
        System.setProperty("apple.awt.UIElement", "true");
    }

    protected abstract void run() throws Exception;
}
