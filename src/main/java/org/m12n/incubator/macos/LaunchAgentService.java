package org.m12n.incubator.macos;

import org.apache.commons.io.IOUtils;
import org.m12n.incubator.properties.AppProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class LaunchAgentService {
    private static final Logger LOG = LoggerFactory.getLogger(LaunchAgentService.class);
    private final Path launchAgentFile;

    public LaunchAgentService(final AppProperties appProperties) {
        final String fileName = String.format("%s.%s.plist", appProperties.getGroup(), appProperties.getId());
        final String launchAgentDir = System.getProperty("user.home") + "/Library/LaunchAgents/";
        launchAgentFile = Paths.get(launchAgentDir, fileName);
    }

    public boolean isRunAtLoad() {
        return Files.exists(launchAgentFile);
    }

    public void toggleRunAtLoad() {
        try {
            if (isRunAtLoad()) {
                Files.delete(launchAgentFile);
            } else {
                final String data = IOUtils.toString(ClassLoader.getSystemResourceAsStream("LaunchAgent.plist"));
                final String replaced = data.replace("$USER_DIR", System.getProperty("user.dir"));
                Files.write(launchAgentFile, replaced.getBytes());
            }
        } catch (final IOException e) {
            LOG.error("Unable to set run at login.", e);
        }
    }
}
