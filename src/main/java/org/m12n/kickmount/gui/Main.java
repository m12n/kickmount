package org.m12n.kickmount.gui;

import org.m12n.incubator.macos.MacOsApplication;
import org.m12n.incubator.macos.MacOsUtils;
import org.m12n.incubator.properties.AppProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

public class Main extends MacOsApplication {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    @Inject
    private TrayMenu trayMenu;
    @Inject
    private AppProperties appProperties;

    public static void main(final String[] args) throws Exception {
        launch(Main.class);
    }

    protected void run() throws Exception {
        LOG.info("Started {} {}.", appProperties.getName(), appProperties.getVersion());

        Runtime.getRuntime().addShutdownHook(new Thread(() -> LOG.info("Stopped {}.", appProperties.getName())));

        final String style = MacOsUtils.isMacMenuBarDarkMode().map(isDarkMode -> isDarkMode ? "_dark" : "").orElse("");
        final URL url = ClassLoader.getSystemResource("icons/menubar" + style + ".png");
        final TrayIcon trayIcon = new TrayIcon(Toolkit.getDefaultToolkit().getImage(url));

        trayIcon.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(final MouseEvent e) {
                trayMenu.update();
            }
        });


        trayIcon.setPopupMenu(trayMenu);
        SystemTray.getSystemTray().add(trayIcon);

        //noinspection InfiniteLoopStatement
        while (true) {
            trayMenu.update();
            TimeUnit.MINUTES.sleep(5);
        }
    }

}
