package org.m12n.kickmount.gui;

import org.m12n.incubator.macos.LaunchAgentService;
import org.m12n.incubator.properties.AppProperties;
import org.m12n.incubator.version.GithubVersionCheck;
import org.m12n.kickmount.volume.Volume;
import org.m12n.kickmount.volume.VolumeModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.CheckboxMenuItem;
import java.awt.EventQueue;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.util.Collection;

import javax.inject.Inject;

class TrayMenu extends PopupMenu {
    private static final String EJECT_SYMBOL = "\u23CF ";
    private static final Logger LOG = LoggerFactory.getLogger(PopupMenu.class);

    @Inject
    private VolumeModel volumeModel;
    @Inject
    private LaunchAgentService launchAgentService;

    private final MenuItem ejectItem;
    private final CheckboxMenuItem runAtLoadItem;
    private final int baseIndex;
    private final int minItemCount;

    TrayMenu(final AppProperties appProperties, final GithubVersionCheck githubVersionCheck) {
        ejectItem = new MenuItem(ejectLabel("Eject All Volumes"));
        ejectItem.addActionListener(e -> doEject());
        add(ejectItem);

        addSeparator();

        baseIndex = getItemCount();

        addSeparator();

        runAtLoadItem = new CheckboxMenuItem("Launch at Login", false);
        runAtLoadItem.addItemListener(e -> doToggleRunAtLoad());
        add(runAtLoadItem);

        if (githubVersionCheck.updateAvailable()) {
            final MenuItem updateItem = new MenuItem("Download Update");
            updateItem.addActionListener(e -> runLater(githubVersionCheck::downloadLatestVersion));
            add(updateItem);
        }

        addSeparator();

        final MenuItem quitItem = new MenuItem("Quit " + appProperties.getName());
        quitItem.addActionListener(e -> {
            LOG.info("Quitting application.");
            System.exit(0);
        });
        add(quitItem);

        minItemCount = getItemCount();
    }

    void update() {
        runLater(this::doUpdate);
    }

    private void doUpdate() {
        if (volumeModel.isChanged()) {
            while (getItemCount() > minItemCount) {
                remove(baseIndex);
            }

            final Collection<Volume> volumes = volumeModel.listVolumes();

            ejectItem.setEnabled(!volumes.isEmpty());

            if (volumes.isEmpty()) {
                final MenuItem item = new MenuItem("No External Volumes");
                item.setEnabled(false);
                insert(item, baseIndex);
            } else {
                int i = baseIndex;
                for (final Volume volume : volumes) {
                    final MenuItem item = new MenuItem(ejectLabel(volume));
                    volume.addEjectFailedListener(failed -> item.setLabel(ejectLabel(volume)));
                    item.addActionListener(e -> doEject(volume));
                    insert(item, i++);
                }
            }

            runAtLoadItem.setState(launchAgentService.isRunAtLoad());
        }
    }

    private void doEject(final Volume volume) {
        runLater(() -> {
            volumeModel.ejectVolume(volume);
            doUpdate();
        });
    }

    private void doEject() {
        runLater(() -> {
            volumeModel.ejectAllVolumes();
            doUpdate();
        });
    }

    private void doToggleRunAtLoad() {
        runLater(() -> {
            launchAgentService.toggleRunAtLoad();
            doUpdate();
        });
    }

    private void runLater(final Runnable runnable) {
        EventQueue.invokeLater(runnable);
    }

    private String ejectLabel(final Volume volume) {
        return ejectLabel(volume.getName() + (volume.isEjectFailed() ? " \u26D4" : ""));
    }

    private String ejectLabel(final String name) {
        return EJECT_SYMBOL + name;
    }
}
