package org.m12n.kickmount.volume;

import org.m12n.incubator.process.ProcessManager;
import org.m12n.incubator.xml.SimpleElementHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

class MountService {
    private static final Logger LOG = LoggerFactory.getLogger(MountService.class);

    @Inject
    private ProcessManager processManager;

    void unmount(final Volume volume) {
        final String volumeName = volume.getName();
        try {
            final Process process = processManager.exec("diskutil", "eject", volumeName);
            final boolean success = process.waitFor(10, TimeUnit.SECONDS);
            if (!success) {
                volume.setEjectFailed(true);
                LOG.warn("Unable to eject volume {}.", volumeName);
            } else {
                final int exitValue = process.exitValue();
                if (exitValue != 0) {
                    LOG.warn("Unable to eject volume {} ({}).", volumeName, exitValue);
                    volume.setEjectFailed(true);
                }
            }
        } catch (final IOException | InterruptedException e) {
            volume.setEjectFailed(true);
            LOG.error("Unable to eject volume " + volumeName + ".", e);
        }
    }

    Collection<Volume> externalVolumes() {
        final Process process;
        try (final InputStream in = (process = processManager.exec("diskutil", "list", "-plist", "external")).getInputStream()) {
            final int exitValue = process.waitFor();
            if (exitValue == 0) {
                final XMLReader xmlReader = XMLReaderFactory.createXMLReader();
                final Collection<Volume> volumes = new ArrayList<>();
                xmlReader.setContentHandler(new SimpleElementHandler() {
                    private boolean readArrayMode = false;

                    @Override
                    protected void element(final String localName, final String characters) {
                        switch (localName) {
                            case "key":
                                readArrayMode = characters.equals("VolumesFromDisks");
                                break;
                            case "string":
                                if (readArrayMode) {
                                    volumes.add(new Volume(characters));
                                }
                                break;
                            case "array":
                                readArrayMode = false;
                                break;
                        }
                    }
                });
                xmlReader.parse(new InputSource(in));
                return volumes;
            }
            LOG.error("Unable to list external volumes ({}).", exitValue);
        } catch (IOException | InterruptedException | SAXException e) {
            LOG.error("Unable to list external volumes.", e);
        }
        return Collections.emptyList();
    }
}
