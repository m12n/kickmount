package org.m12n.kickmount.volume;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.inject.Inject;

public class VolumeModel {
    @Inject
    private MountService mountService;

    private Collection<Volume> mountedVolumes;

    public boolean isChanged() {
        final Collection<Volume> volumes = mountService.externalVolumes();

        if (volumes.equals(mountedVolumes)) {
            return false;
        }

        if (mountedVolumes != null) {
            final Map<String, Volume> volumeMap = mountedVolumes.stream()
                    .collect(Collectors.toMap(Volume::getName, Function.identity()));

            volumes.forEach(volume -> {
                final Volume mountedVolume = volumeMap.get(volume.getName());
                if (mountedVolume != null) {
                    volume.setEjectFailed(mountedVolume.isEjectFailed());
                }
            });
        }

        mountedVolumes = volumes;
        return true;
    }

    public void ejectVolume(final Volume volume) {
        mountService.unmount(volume);
    }

    public void ejectAllVolumes() {
        mountedVolumes.forEach(mountService::unmount);
    }

    public Collection<Volume> listVolumes() {
        return mountedVolumes;
    }
}
